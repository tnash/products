CREATE TRIGGER myyesgo_integration.products_updated
    BEFORE
UPDATE
    ON products
    FOR EACH ROW
BEGIN
SET new.updated_dt = NOW();
END;
