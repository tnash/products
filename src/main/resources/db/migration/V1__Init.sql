CREATE TABLE applications
(
    id INT UNSIGNED auto_increment,
    primary key (id)
);

CREATE TABLE product_reference
(
    id              INT UNSIGNED auto_increment,
    reference_type  VARCHAR(10)  NOT NULL,
    reference_code  VARCHAR(20)  NOT NULL,
    reference_value VARCHAR(20)  NOT NULL,
    is_active   SMALLINT NOT NULL DEFAULT 1,
    created_by      VARCHAR(255) NOT NULL,
    updated_by      VARCHAR(255) NOT NULL,
    created_dt      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (id)
);

CREATE TABLE products
(
    id                    INT UNSIGNED auto_increment,
    category_reference_id INT UNSIGNED NOT NULL,
    product_name          VARCHAR(100) NOT NULL,
    product_description   VARCHAR(200),
    is_active   SMALLINT NOT NULL DEFAULT 1,
    created_by            VARCHAR(255) NOT NULL,
    updated_by            VARCHAR(255) NOT NULL,
    created_dt            DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt            DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (id),
    constraint fk_p_product_reference FOREIGN KEY (category_reference_id) REFERENCES product_reference (id)
);

CREATE TABLE product_variations
(
    id                    INT UNSIGNED auto_increment,
    product_id            INT UNSIGNED NOT NULL,
    supplier_reference_id INT UNSIGNED NOT NULL,
    variation_name        VARCHAR(100) NOT NULL,
    variation_description VARCHAR(250),
    variation_base_cost   DECIMAL(15, 2),
    variation_base_markup DECIMAL(15, 2),
    is_active   SMALLINT NOT NULL DEFAULT 1,
    created_by            VARCHAR(255) NOT NULL,
    updated_by            VARCHAR(255) NOT NULL,
    created_dt            DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt            DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (id),
    CONSTRAINT fk_pv_product FOREIGN KEY (product_id) REFERENCES products (id),
    constraint fk_pv_product_reference FOREIGN KEY (supplier_reference_id) REFERENCES product_reference (id)
);

CREATE TABLE product_variation_modifiers
(
    id                      INT UNSIGNED auto_increment,
    product_variation_id    INT UNSIGNED NOT NULL,
    modifier_name           VARCHAR(100) NOT NULL,
    modifier_description    VARCHAR(250),
    modifier_base_cost_adder DECIMAL(15, 2),
    is_active   SMALLINT NOT NULL DEFAULT 1,
    created_by              VARCHAR(255) NOT NULL,
    updated_by              VARCHAR(255) NOT NULL,
    created_dt              DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt              DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT fk_pvm_product_variation FOREIGN KEY (product_variation_id) REFERENCES product_variations (id)
);

CREATE TABLE application_product_variations
(
    id                                INT UNSIGNED auto_increment,
    application_id                    INT UNSIGNED NOT NULL,
    product_variation_id              INT UNSIGNED NOT NULL,
    application_variation_base_cost   DECIMAL(15, 2),
    application_variation_base_markup DECIMAL(15, 2),
    created_by                        VARCHAR(255) NOT NULL,
    updated_by                        VARCHAR(255) NOT NULL,
    created_dt                        DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt                        DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT fk_apv_product_variation FOREIGN KEY (product_variation_id) REFERENCES product_variations (id)
);

CREATE TABLE application_product_variation_modifiers
(
    id                               INT UNSIGNED auto_increment,
    application_product_variation_id INT UNSIGNED NOT NULL,
    product_variation_modifier_id    INT UNSIGNED NOT NULL,
    application_modifier_cost_adder  DECIMAL(15, 2) NOT NULL,
    created_by                       VARCHAR(255)   NOT NULL,
    updated_by                       VARCHAR(255)   NOT NULL,
    created_dt                       DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_dt                       DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (id),
    CONSTRAINT fk_apvm_application_product_variation FOREIGN KEY (application_product_variation_id) REFERENCES application_product_variations (id),
    CONSTRAINT fk_apvm_product_variation_modifier FOREIGN KEY (product_variation_modifier_id) REFERENCES product_variation_modifiers (id)
);

INSERT INTO product_reference (reference_type, reference_code, reference_value, created_by, updated_by)
VALUES ('CATEGORY', 'GAP', 'Gap', 'SYSTEM', 'SYSTEM');

INSERT INTO product_reference (reference_type, reference_code, reference_value, created_by, updated_by)
VALUES ('CATEGORY', 'ESC', 'ESC', 'SYSTEM', 'SYSTEM');

INSERT INTO product_reference (reference_type, reference_code, reference_value, created_by, updated_by)
VALUES ('CATEGORY', 'MAINTENANCE', 'MAINTENANCE', 'SYSTEM', 'SYSTEM');

INSERT INTO product_reference (reference_type, reference_code, reference_value, created_by, updated_by)
VALUES ('SUPPLIER', 'BMW', 'BMW', 'SYSTEM', 'SYSTEM');

INSERT INTO product_reference (reference_type, reference_code, reference_value, created_by, updated_by)
VALUES ('SUPPLIER', 'VERITAS', 'Veritas', 'SYSTEM', 'SYSTEM');

INSERT INTO products (category_reference_id, product_name, product_description, created_by, updated_by)
SELECT pr.id, 'BMW Maintenance Agreement', 'BMW maintenance agreement', 'SYSTEM', 'SYSTEM' FROM product_reference pr WHERE pr.reference_type = 'CATEGORY' AND pr.reference_code = 'MAINTENANCE';

INSERT INTO product_variations (product_id, supplier_reference_id, variation_name, variation_description, variation_base_cost,
                                variation_base_markup, created_by, updated_by)
SELECT p.id, (SELECT pr.id FROM product_reference pr WHERE pr.reference_type = 'SUPPLIER' AND pr.reference_code = 'BMW' AND pr.is_active = 1), 'BMW Vehicle Maintenance Refresh', 'Refresh of an existing BMW maintenance agreement', 0.0, 0.0, 'SYSTEM', 'SYSTEM'
FROM products p
WHERE p.product_name = 'BMW Maintenance Agreement';

INSERT INTO product_variations (product_id, supplier_reference_id, variation_name, variation_description, variation_base_cost,
                                variation_base_markup, created_by, updated_by)
SELECT p.id,
       (SELECT pr.id FROM product_reference pr WHERE pr.reference_type = 'SUPPLIER' AND pr.reference_code = 'BMW' AND pr.is_active = 1),
       'BMW Vehicle Maintenance Upgrade/Extension',
       'Upgrade or extend an existing BMW maintenance agreement',
       0.0,
       0.0,
       'SYSTEM',
       'SYSTEM'
FROM products p
WHERE p.product_name = 'BMW Maintenance Agreement';

INSERT INTO application_product_variations(application_id, product_variation_id, application_variation_base_cost,
                                           application_variation_base_markup, created_by, updated_by)
SELECT a.id, pv.id, 1250.45, 200.00, 'SYSTEM', 'SYSTEM'
FROM applications a,
     product_variations pv
WHERE variation_name = 'BMW Vehicle Maintenance Refresh';