package com.ysg.product.repository;

import com.ysg.product.model.ProductReference;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Get data from the product_references table.
 */
public interface ProductReferenceRepository extends JpaRepository<ProductReference, Long> {
    /**
     * Return product references matching the reference code. Should be only 1.
     * @param referenceCode
     * @return
     */
    List<ProductReference> findAllByReferenceCode(String referenceCode);

    /**
     * Return product references matching the reference type.
     * @param referenceType
     * @return
     */
    List<ProductReference> findAllByReferenceType(String referenceType);
}
