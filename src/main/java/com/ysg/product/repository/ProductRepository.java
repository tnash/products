package com.ysg.product.repository;

import com.querydsl.core.types.Predicate;
import com.ysg.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

/**
 * Query Products table.
 */
public interface ProductRepository extends JpaRepository<Product, Long>,
        QuerydslPredicateExecutor<Product> {

    /**
     * Find all products matching the product name.
     */
    List<Product> findAllByProductName(String productName);


    /**
     * Find all products of a product type. (e.g. Gap, ESC, etc)
     *
     * @param productType
     * @return
     */
    List<Product> findAllByProductReferenceReferenceType(String productType);

    /**
     * Find all products of a product reference code. (e.g. BMW, VERITAS)
     *
     * @param productCode
     * @return
     */
    List<Product> findAllByProductReferenceReferenceCode(String productCode);

    /**
     * Use QueryDSL library to query products using predicates.
     * Find all products that match the specified predicate.
     *
     * @param predicate
     * @return
     */
    List<Product> findAllBy(Predicate predicate);

}
