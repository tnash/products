package com.ysg.product.service;

import com.google.common.base.Strings;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.ysg.product.dataaccess.converters.ProductMapper;
import com.ysg.product.dataaccess.dto.ProductDTO;
import com.ysg.product.dataaccess.filters.ProductFilter;
import com.ysg.product.dataaccess.predicates.ProductPredicateBuilder;
import com.ysg.product.model.Product;
import com.ysg.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Manage products.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService extends JpaServiceBase<Product, Long> {
    private final ProductRepository productRepository;

    private static ProductMapper mapper = ProductMapper.INSTANCE;

    /**
     * Return the JpaRepository instance.
     * @return
     */
    @Override
    protected JpaRepository<Product, Long> getRepository() {
        return productRepository;
    }

    // Service methods to query the data here.

    /**
     * Return a list of products matching the criteria defined in the filter.
     *
     * @param filter The criteria to search for matching assessments.
     * @return A list of assessments.
     */
    public List<ProductDTO> find(final ProductFilter filter) {
        List<Product> products;
        if (Objects.isNull(filter) || filter.isEmpty()) {
            products = getRepository().findAll();
        } else {
            Predicate predicate = buildPredicate(filter);
            // note must use the injected instance directory so as not to get the JpaRespository type.
            products = productRepository.findAllBy(predicate);
        }
        return convertEntityToDto(products);
    }

    /**
     * A builder to construct a finder predicate for use in locating products with variable search parameters.
     *
     * @param filter The filter object to apply
     * @return The predicate query to execute
     */
    private BooleanExpression buildPredicate(final ProductFilter filter) {
        if (Objects.isNull(filter)) {
            throw new IllegalArgumentException("Product filter cannot be null");
        }
        ProductPredicateBuilder builder = new ProductPredicateBuilder();

        builder = Strings.isNullOrEmpty(filter.getProductName()) ? builder
                : builder.with("productName", "startsWith", filter.getProductName());

        builder = Strings.isNullOrEmpty(filter.getProductCode()) ? builder
                : builder.with("productReference.productCode", "eq", filter.getProductCode());

        builder = Strings.isNullOrEmpty(filter.getProductType()) ? builder
                : builder.with("productReference.productType", "eq", filter.getProductType());

        builder = Objects.isNull(filter.getActive()) ? builder
                : builder.with("active", "eq", filter.getActive());

        return builder.build();
    }

    private List<ProductDTO> convertEntityToDto(List<Product> productEntities) {
        return productEntities.stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }
}

