package com.ysg.product.service;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.Optional;

/**
 * This a helper class that offers some default methods to operate on objects via repositories.
 *
 * @param <T> The Type of the entity class.
 * @param <I> The Type of the entity class idenfifier.
 */
public abstract class JpaServiceBase<T, I extends Serializable> {

    /**
     * The dataaccess to support the object.
     *
     * @return The dataaccess.
     */
    protected abstract JpaRepository<T, I> getRepository();

    /**
     * This will return the object based on the id.
     *
     * @param id The id to search for
     * @return The object with the passed in id.
     */
    public Optional<T> get(final I id) {
        return getRepository().findById(id);
    }

    /**
     * This will delete the object in question.
     *
     * @param id The id of the object to delete
     */
    public void delete(final I id) {
        getRepository().deleteById(id);
    }

    /**
     * This will save the object in question.
     *
     * @param object The Object to save
     * @return The object that was saved
     */
    public T save(final T object) {
        return getRepository().save(object);
    }

}
