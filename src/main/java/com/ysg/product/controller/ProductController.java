package com.ysg.product.controller;

import com.ysg.product.dataaccess.dto.ProductDTO;
import com.ysg.product.dataaccess.filters.ProductFilter;
import com.ysg.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The Api for managing lender information.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    // Other methods to manage products

    @ApiOperation(value = "Find products that matches the filter criteria specified in the request.", response = LenderDTO.class,
            notes = "Find products matching the filter criteria.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success", response = Object.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "A server failure has occurred.")})
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ProductDTO> find(@RequestParam(name = "productName", required = false) final String productName,
                                 @RequestParam(name = "productCode", required = false) final String productCode,
                                 @RequestParam(name = "productType", required = false) final String productType,
                                 @RequestParam(name = "active", required = false) final Boolean active) {
        ProductFilter productFilter = new ProductFilter(productName, productType, productCode, active);
        return productService.find(productFilter);
    }
}
