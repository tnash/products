package com.ysg.product.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A reference type that defines the acceptable values for product category, product supplier, etc.
 */
@Entity
@Table(name = "product_references")
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductReference extends AbstractAuditableDomain {
    @Column(name = "reference_type")
    private String referenceType;

    @Column(name = "reference_code")
    private String referenceCode;

    @Column(name = "is_active")
    private Boolean isActive;
}
