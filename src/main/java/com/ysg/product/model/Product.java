package com.ysg.product.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * A Product that can be added to a loan deal.
 */
@Entity
@Table(name = "products")
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Product extends AbstractAuditableDomain{
    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "is_active")
    private Boolean isActive;

    @ManyToOne
    private ProductReference productReference;

//    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    private List<ProductVariation> productVariations;

    /**
     * Add a product variation.
     */
//    public Product addProductVariation(final ProductVariation productVariation) {
//        if (Objects.isNull(productVariations)) {
//            productVariations = new ArrayList<>();
//        }
//
//        this.productVariations.add(productVariation);
//        productVariation.addProduct(this);
//        return this;
//    }
}
