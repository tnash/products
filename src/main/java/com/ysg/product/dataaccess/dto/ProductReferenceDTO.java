package com.ysg.product.dataaccess.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product data object that is exposed to clients.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductReferenceDTO {
    private String referenceType;
    private String referenceCode;
    private Boolean isActive;
}
