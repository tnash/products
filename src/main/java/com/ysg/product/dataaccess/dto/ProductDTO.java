package com.ysg.product.dataaccess.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product data object that is exposed to clients.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDTO {
    private String productName;
    private String productDescription;
    private Boolean isActive;
    @JsonIgnore
    private ProductReferenceDTO productReferenceDTO;
}
