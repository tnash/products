package com.ysg.product.dataaccess.filters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * A filter for Product predicates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductFilter {
    private String productName;
    private String productType;
    private String productCode;
    private Boolean active;

    public boolean isEmpty() {
        return Objects.isNull(productName)
                && Objects.isNull(active)
                && Objects.isNull(productType)
                && Objects.isNull(productCode);
    }
}
