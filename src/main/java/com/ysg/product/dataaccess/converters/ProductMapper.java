package com.ysg.product.dataaccess.converters;

import com.ysg.product.dataaccess.dto.ProductDTO;
import com.ysg.product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper to convert Product entities to ProductDTOs.
 */
@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    /**
     * Convert the entity to a DTO.
     *
     * @param source
     * @return
     */
    ProductDTO entityToDto(Product source);

    /**
     * Convert the DTO to an entity.
     *
     * @param source
     * @return
     */
    Product dtoToEntity(ProductDTO source);
}
