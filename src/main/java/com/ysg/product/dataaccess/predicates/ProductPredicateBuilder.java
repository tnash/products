package com.ysg.product.dataaccess.predicates;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.ysg.product.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder for creating Product predicates with search criteria terms.
 */
public class ProductPredicateBuilder {
    private List<SearchCriteria> params;

    /**
     * Default constructor.
     */
    public ProductPredicateBuilder() {
        params = new ArrayList<>();
    }

    /**
     * Build the search criteria with the specified key (column name), operation (eq, > ,<)
     * and value.
     *
     * @param key
     * @param operation
     * @param value
     * @return
     */
    public ProductPredicateBuilder with(final String key, final String operation, final Object value) {

        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    /**
     * Build the BooleanExpression.
     *
     * @return
     */
    public BooleanExpression build() {
        if (params.size() == 0) {
            return null;
        }

        List<BooleanExpression> predicates = new ArrayList<>();

        for (SearchCriteria param : params) {
            BooleanExpression exp = Predicates.getPredicate(Product.class, "product", param);
            if (exp != null) {
                predicates.add(exp);
            }
        }

        BooleanExpression result = predicates.get(0);
        for (int i = 1; i < predicates.size(); i++) {
            result = result.and(predicates.get(i));
        }
        return result;
    }
}
