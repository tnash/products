package com.ysg.product.dataaccess.predicates;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The SearchCriteria holds the details we need to represent a constraint:
 * <p>
 * key: the field name – for example: firstName, age, … etc
 * operation: the operation – for example: Equality, less than, … etc
 * value: the field value – for example: john, 25, … etc.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
class SearchCriteria {
    private String key;
    private String operation;
    private Object value;
}
