package com.ysg.product.dataaccess.predicates;


import com.querydsl.core.types.dsl.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.GenericValidator;

import java.time.LocalDate;

import static org.apache.commons.lang3.StringUtils.isNumeric;

/**
 * Build QueryDsl predicates.
 */
@Slf4j
public final class Predicates {

    /**
     * Hide the constructor.
     */
    private Predicates() {
    }

    /**
     * Return the represented predicate.
     *
     * @return BooleanExpression
     */
    @SuppressWarnings("unchecked")
    static BooleanExpression getPredicate(final Class entity, final String predicatePath, final SearchCriteria criteria) {
        PathBuilder entityPath = new PathBuilder(entity, predicatePath);

        if (isNumeric(criteria.getValue().toString())) {
            NumberPath<Long> path = entityPath.getNumber(criteria.getKey(), Long.class);
            long value = Long.parseLong(criteria.getValue().toString());
            switch (criteria.getOperation()) {
                case ":":
                case "eq":
                case "equals":
                    return path.eq(value);
                case "ge":
                case ">=":
                    return path.goe(value);
                case "le":
                case "<=":
                    return path.loe(value);
                default:
                    log.info("Unrecognized predicate operation {}", criteria.getOperation());
                    return null;
            }
        } else if (GenericValidator.isDate(criteria.getValue().toString(), "yyyy-MM-dd", true)) {
            DatePath path = entityPath.getDate(criteria.getKey(), LocalDate.class);
            LocalDate value = LocalDate.parse(criteria.getValue().toString());
            switch (criteria.getOperation()) {
                case ":":
                case "eq":
                case "equals":
                    return path.eq(value);
                case "ge":
                case ">=":
                    return path.goe(value);
                case "le":
                case "<=":
                    return path.loe(value);
                default:
                    log.info("Unrecognized predicate operation {}", criteria.getOperation());
                    return null;
            }
        } else {
            StringPath path = entityPath.getString(criteria.getKey());
            switch (criteria.getOperation()) {
                case "eq":
                case "=":
                case "equals":
                    return path.equalsIgnoreCase(criteria.getValue().toString());
                case ":":
                case "like":
                case "contains":
                    return path.containsIgnoreCase(criteria.getValue().toString());
                case "startsWith":
                    return path.startsWithIgnoreCase(criteria.getValue().toString());
                default:
                    log.info("Unrecognized predicate operation {}", criteria.getOperation());
                    return null;
            }
        }
    }
}
